Installing dependencies
=================

source env/bin/activate

pip install -r requirements.txt
python3 -m pip install -r requirements.txt

create .env file
SET MONGO_CONNECTION = "<YOUR MONGO CONNECTION>"

cd app
python training.py

python main.py