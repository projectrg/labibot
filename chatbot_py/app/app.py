from flask import Flask
from flask_bootstrap import Bootstrap
import os
from decouple import config



app = Flask(__name__)
app.config.from_pyfile('flask.cfg')
Bootstrap(app)