from pymongo import MongoClient
from bson.objectid import ObjectId
import datetime
from decouple import config


client = MongoClient(config('MONGO_CONNECTION'))

                         

db = client.labibot
i_list = []


def get_json_intents():
    intents = db.intents
    results = intents.find({},{"_id": 0})

    for result in results:
        i_list.append(result)

    return {"intents": i_list}


def get_count_login(username, password):
    login = db.login
    return login.find({"username": username, "pass": password},{"_id": 0}).count()


def insert_log_records(session_id, status_code, flg_entity, s_user, text):
    records = db.records

    r_session = records.find_one({"session_id": session_id})
    content = {"status_code": status_code, "entity": "User/{}".format(s_user) if flg_entity == True else "Bot", "text": text}

    if r_session is None:
        template = {"session_id": session_id, "date": datetime.datetime.utcnow(), "content": [content]}
        db.records.insert_one(template)
    else:
        r_session['content'].append(content)
        records.replace_one({"session_id": session_id}, r_session, upsert=True)
        #print(dumps(db.testcollection.find_one({}, {'_id': 0}), indent=4))
