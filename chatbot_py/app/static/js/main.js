
(function ($) {
    "use strict";

    /*==================================================================
    [ Focus Contact2 ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(e) {
        e.preventDefault();

        var bool = true;
        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                bool = false;
            }
        }

        var form = $(this);
        var url = form.attr('action');
        var s_username = $('form input[name="email"]').val();
        var json_cred = 
            {"email": s_username, 
            "passw": $('form input[name="passw"]').val()};

        if(bool) {
            $.ajax({
                type: "POST",
                url: url,
                data: JSON.stringify(json_cred),
                contentType: "application/json",
                dataType: 'json',
                success: function(result) {
                    console.log(result.status);
                    if(result.status === 0) {
                        sessionStorage.setItem("s_user", s_username);
                        window.location.href = "/chatbot";
                    } else {
                        alert("Login incorrecto. Pruebe de nuevo.");
                    }
                }
              });
            ;
        }
    });

    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
    }

})(jQuery);