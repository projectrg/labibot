from flask import Blueprint, Flask, flash, redirect, render_template, request, url_for, jsonify
from flask_wtf import FlaskForm
from wtforms import DateField, StringField, SubmitField, SelectField, ValidationError
from wtforms.validators import DataRequired, Optional
from jobs.actions import *
from models.connection import insert_log_records
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


chatbot_blueprint = Blueprint('chatbot', __name__)


@chatbot_blueprint.route('/', methods=['GET', 'POST'])
def home():
    return render_template('chatbot.html')


@chatbot_blueprint.route("/chatting", methods=['GET', 'POST'])
def get_bot_response():
    s_user = request.args.get('s_user')
    session_id = request.args.get('session_id')

    userText = request.args.get('msg')
    insert_log_records(session_id, "200 OK", True, s_user, userText)

    response = chatbot_response(userText)
    insert_log_records(session_id, "200 OK", False, s_user, response[0]["res"])

    return jsonify(response)

@chatbot_blueprint.route("/sendingmail", methods=['GET', 'POST'])
def send_email():
    results = {}
    if request.method == "POST":
        json_form = request.get_json()
        print(json_form)

        msg = MIMEMultipart()
        msg['Subject'] = json_form['titulo']
        msg["From"] = json_form['nombre'] + " <"+json_form['correo']+">"
        # Registramos el correo del emisor
        msg["To"] = "IntegramedicaSI@gmail.com"
        msg['Reply-To'] = json_form['correo']
        body_text = "From: " + json_form['nombre'] + " <"+json_form['correo']+">" + "\n" + json_form['titulo'] + "\n\n" + json_form['descripcion']
        body_part = MIMEText(body_text, 'plain')
        msg.attach(body_part)
        with smtplib.SMTP(host="smtp.gmail.com", port=587) as smtp_obj:  # ENVIAR DESDE UN DOMINIO PERSONALIZADO.
            smtp_obj.starttls()
            smtp_obj.ehlo()
            smtp_obj.login("IntegramedicaSI@gmail.com", "1nt3gr4m3d1c4")
            smtp_obj.sendmail(msg['From'], [msg['To'],], msg.as_string())

        print("¡Datos enviados con éxito!")
        results = {'status': 0}

    return jsonify(results)

