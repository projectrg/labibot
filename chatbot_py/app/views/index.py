# -*- coding: utf-8 -*-
from models.connection import get_count_login
from flask import Blueprint, Flask, flash, redirect, render_template, request, url_for, jsonify
from flask_wtf import FlaskForm
from wtforms import DateField, StringField, SubmitField, SelectField, ValidationError
from wtforms.validators import DataRequired, Optional
import json


index_blueprint = Blueprint('index', __name__)


@index_blueprint.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@index_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    results = {}
    if request.method == "POST":
        json_cred = request.get_json()
        user_exists = get_count_login(json_cred['email'], json_cred['passw'])

        if user_exists > 0:
            results = {'status': 0}
        else:
            results = {'status': 1}

    return jsonify(results)
