# -*- coding: utf-8 -*-
from app import app
from views import *
from jobs import *
from models import *


# Blueprints

app.register_blueprint(index_blueprint, url_prefix='/')
app.register_blueprint(chatbot_blueprint, url_prefix='/chatbot')


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=8080)