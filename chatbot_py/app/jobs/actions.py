from app import app
from models.connection import get_json_intents
import nltk
from nltk.stem import WordNetLemmatizer
import pickle
import numpy as np
from keras.models import load_model
import json
import random

nltk.download('popular')
lemmatizer = WordNetLemmatizer()
model = load_model(app.config['UTILS_ROOT'] + '/model.h5')

#intents = json.loads(open(app.config['UTILS_ROOT'] + '/data.json').read())
intents = get_json_intents()
words = pickle.load(open(app.config['UTILS_ROOT'] + '/texts.pkl','rb'))
classes = pickle.load(open(app.config['UTILS_ROOT'] + '/labels.pkl','rb'))


def clean_up_sentence(sentence):
    # Tokenizamos el patron(partimos la oracion en palabras y las colocamos dentro de un array)
    sentence_words = nltk.word_tokenize(sentence)
    # Cada palabra pasa por el proceso de stemming que involucra crear variaciones de la misma tomando como parametro la raiz semantica.
    sentence_words = [lemmatizer.lemmatize(word.lower()) for word in sentence_words]
    return sentence_words

# Se retorna un arreglo bolsa de palabras donde se colocara al array valores de 0 y 1 en caso este se ubique dentro de la bolsa

def bow(sentence, words, show_details=True):
    # Tokenizamos el patron
    sentence_words = clean_up_sentence(sentence)
    # Bolsa de palabras - matriz de N palabras
    bag = [0]*len(words)  
    for s in sentence_words:
        for i,w in enumerate(words):
            if w == s: 
                # Asignamos 1 si la palabra ubicada esta en posicion dentro de la bolsa
                bag[i] = 1
                if show_details:
                    print ("found in bag: %s" % w)
    return(np.array(bag))

def predict_class(sentence, model):
    # Filtrar las predicciones dentro del margen de error
    p = bow(sentence, words,show_details=False)
    res = model.predict(np.array([p]))[0]
    ERROR_THRESHOLD = 0.15
    results = [[i,r] for i,r in enumerate(res) if r>ERROR_THRESHOLD]
    # Ordenamos tomando en cuenta la mayor probabilidad
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
    return return_list

def getResponse(ints, intents_json):
    tag = ints[0]['intent']
    list_of_intents = intents_json['intents']
    for i in list_of_intents:
        if(i['tag']== tag):
            result = random.choice(i['responses'])
            break
    return result

def chatbot_response(msg):
    details_list = []
    # mientras mas margen de error, mas tags se asimilaran
    ints = predict_class(msg, model)
    # respuesta del bot en string
    res = getResponse(ints, intents)

    details_list.append({"res": res, "ints": ints[0]['intent']})
    return details_list