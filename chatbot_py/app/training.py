from app import app
from models.connection import get_json_intents
import nltk
from nltk.stem import WordNetLemmatizer
import json
import pickle
import os

import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from tensorflow.keras.optimizers import SGD
import random


lemmatizer = WordNetLemmatizer()

words=[]
classes = []
documents = []
ignore_words = ['?', '!']
#data_file = open(app.config['UTILS_ROOT'] + '/data.json').read()
#intents = json.loads(data_file)
intents = get_json_intents()


for intent in intents['intents']:
    for pattern in intent['patterns']:

        # Tokenizamos las oraciones
        w = nltk.word_tokenize(pattern)
        words.extend(w)
        # añadimosel documento dentro del corpus
        documents.append((w, intent['tag']))

        # añadimos a nuestra lista de clases 
        if intent['tag'] not in classes:
            classes.append(intent['tag'])

# Lematizamos y cambiamos cada palabra a minusculas a fin de remover los duplicados 
words = [lemmatizer.lemmatize(w.lower()) for w in words if w not in ignore_words]
words = sorted(list(set(words)))
# Ordenamos las clases 
classes = sorted(list(set(classes)))
# documents = viene a ser la combinacion entre intents y responses
print (len(documents), "documents")
# classes = intents
print (len(classes), "classes", classes)
# words = todas las palabras dentro del arreglo
print (len(words), "unique lemmatized words", words)


pickle.dump(words,open(app.config['UTILS_ROOT'] + '/texts.pkl','wb'))
pickle.dump(classes,open(app.config['UTILS_ROOT'] + '/labels.pkl','wb'))

# creamos nuestra data de entrenamiento
training = []
# creamos un array vacio para la salida
output_empty = [0] * len(classes)
# Entrenamos el set junto a la bolsa de palabras por cada oracion
for doc in documents:
    # Inicializamos nuestra bolsa de palabras
    bag = []
    # listamos las palabras tokenizadas dentro del patron
    pattern_words = doc[0]
    # Lematizamos cada palabra creando sus variaciones
    pattern_words = [lemmatizer.lemmatize(word.lower()) for word in pattern_words]
    # creamos nuestro arreglo de bolsa de palabras colocando 1 en caso esta exista dentro de la bolsa.
    for w in words:
        bag.append(1) if w in pattern_words else bag.append(0)
    
    # Salidas --> 0 para el tag no correcto y 1 para el tag adecuado
    output_row = list(output_empty)
    output_row[classes.index(doc[1])] = 1
    
    training.append([bag, output_row])
# Mezclamos las caracteristicas y las insertamos en el array .np
random.shuffle(training)
training = np.array(training, dtype=object)
# Creamos los datos de entrenamiento ---> X para los patrones , Y para los intents
train_x = list(training[:,0])
train_y = list(training[:,1])
print("Training data created")



# Creamos un modelo de 3 capas. La primera de 128 neuronas, la segunda de 64 neuronas y la tercera contiene la cantidad de neuronas correspondientes a los intents predecidos bajo la funcion SoftMax
model = Sequential()
model.add(Dense(128, input_shape=(len(train_x[0]),), activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(len(train_y[0]), activation='softmax'))

# Para compilar este modelo se uso la bajada estocastica de gradiente por lo buenos resultados que ofrece.
sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])

#Guardamos el modelo
hist = model.fit(np.array(train_x), np.array(train_y), epochs=200, batch_size=5, verbose=1)
model.save(app.config['UTILS_ROOT'] + '/model.h5', hist)

print("model created")